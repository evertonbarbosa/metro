<?php

include('config.php');
$mysqli = new mysqli($db['host'], $db['user'], $db['pass'], $db['name']);
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
}

$result = $mysqli->query("SELECT * FROM Linha");

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $linhas[] = $row;
    }
} else {
    $linhas = null;
}

$result = $mysqli->query("SELECT Estacoes.idEstacoes, Estacoes.nome, Linha.nome as nomeLinha FROM Estacoes INNER JOIN Linha ON Estacoes.linha=Linha.idLinha ORDER BY Estacoes.idEstacoes;");

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $estacoes[] = $row;
    }
} else {
    $estacoes = null;
}

$result = $mysqli->query("SELECT a.nome as estacao1, c.nome as linha1, b.nome as estacao2, d.nome as linha2 FROM Conexoes INNER JOIN Estacoes a ON Conexoes.estacao1=a.idEstacoes INNER JOIN Estacoes b ON Conexoes.estacao2=b.idEstacoes INNER JOIN Linha c ON a.linha=c.idLinha INNER JOIN Linha d ON b.linha=d.idLinha");
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $conexoes[] = $row;
    }
} else {
    $estacoes = null;
}

$mysqli->close();

?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>Grafos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Trabalho de Grafos - 2014/2"/>
        <meta name="author" content="EJ-kun"/>
        <link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css"    media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="dist/css/all.css"    media="screen,projection"/>
    </head>

    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        <a class="navbar-brand" href="#"><b>Grafos</b></a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Aplicação</a></li>
                            <li><a href="new_page.php">Cadastrar</a></li>
                            <li class="active"><a href="view_page.php">Visualizar</a></li>
                        </ul>
                        <p class="navbar-text navbar-right">Por Everton Júnior e Thiago Baltazar</p>
                    </div>
                </div>
            </nav>
            <div class="container panel panel-default page">
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default aside">
                            <h3 style="text-align:center;">O que deseja visualizar?</h3>
                            <br>
                            <ul class="nav nav-pills nav-stacked">
                                <li role="presentation" class="type_form active"><a id="show_linha">Linhas de Metrô</a></li>
                                <li role="presentation" class="type_form"><a id="show_estacao">Estações de Metrô</a></li>
                                <li role="presentation" class="type_form"><a id="show_conexao">Conexões entre Estações</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel panel-default main">
                            <div class="linha">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome da Linha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $i=1;
                                            foreach ($linhas as $linha) {
                                                echo "<tr>";
                                                echo "<td>".$i++."</td>";
                                                echo "<td>".$linha['nome']."</td>";
                                                echo "</tr>";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="estacao">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome da Estação</th>
                                            <th>Linha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $i=1;
                                            foreach ($estacoes as $estacao) {
                                                echo "<tr>";
                                                echo "<td>".$i++."</td>";
                                                echo "<td>".$estacao['nome']."</td>";
                                                echo "<td>".$estacao['nomeLinha']."</td>";
                                                echo "</tr>";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="conexao">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Estação 1</th>
                                            <th>Estação 2</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $i=1;
                                            foreach ($conexoes as $conexao) {
                                                echo "<tr>";
                                                echo "<td>".$i++."</td>";
                                                echo "<td>".$conexao['estacao1'].' - '.$conexao['linha1']."</td>";
                                                echo "<td>".$conexao['estacao2'].' - '.$conexao['linha2']."</td>";
                                                echo "</tr>";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="dist/js/jquery1.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            var linha = $(".linha");
            var estacao = $(".estacao");
            var conexao = $(".conexao");

            $(function() {
                estacao.hide();
                conexao.hide();
            });

            $("#show_linha").on('click', function () {
                estacao.slideUp();
                conexao.slideUp();
                linha.show("slow");
                $(".type_form").removeClass("active");
                $(this).parent().addClass("active");
            });

            $("#show_estacao").on('click', function () {
                linha.slideUp();
                conexao.slideUp();
                estacao.show("slow");
                $(".type_form").removeClass("active");
                $(this).parent().addClass("active");
            });

            $("#show_conexao").on('click', function () {
                linha.slideUp();
                estacao.slideUp();
                conexao.show("slow");
                $(".type_form").removeClass("active");
                $(this).parent().addClass("active");
            });
        </script>
    </body>
</html>