<?php

include('config.php');
$mysqli = new mysqli($db['host'], $db['user'], $db['pass'], $db['name']);
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
}

$result = $mysqli->query("SELECT * FROM Linha");

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $linhas[] = $row;
    }
} else {
    $linhas = null;
}

$result = $mysqli->query("SELECT Estacoes.idEstacoes, Estacoes.nome, Linha.nome as nomeLinha FROM Estacoes INNER JOIN Linha ON Estacoes.linha=Linha.idLinha ORDER BY Linha.nome;");

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $estacoes[] = $row;
    }
} else {
    $estacoes = null;
}

$mysqli->close();

?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>Grafos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Trabalho de Grafos - 2014/2"/>
        <meta name="author" content="EJ-kun"/>
        <link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css"    media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="dist/css/all.css"    media="screen,projection"/>
    </head>

    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        <a class="navbar-brand" href="#"><b>Grafos</b></a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php">Aplicação</a></li>
                            <li><a href="new_page.php">Cadastrar</a></li>
                            <li><a href="view_page.php">Visualizar</a></li>
                        </ul>
                        <p class="navbar-text navbar-right">Por Everton Júnior e Thiago Baltazar</p>
                    </div>
                </div>
            </nav>
            <div class="container panel panel-default page">
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default aside">
                            <h3 style="text-align:center;">Qual algoritmo deseja usar?</h3>
                                <br>
                                <ul class="nav nav-pills nav-stacked">
                                    <li role="presentation" class="type_form active"><a id="dijkstra">Djikstra</a></li>
                                </ul>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel panel-default main">
                            <input hidden type="text" name="algoritmo" value="dijkstra">
                            <form id="formulario" action="dijkstra.php" method="post">
                                <h4>Procurar menor rota entre estações</h4>
                                <br>
                                <div class="form-group">
                                    <label for="select_estacao1">Escolha uma Estação</label>
                                    <select required name="inicio" id="select_estacao1">
                                        <?php 
                                            foreach ($estacoes as $estacao) {
                                                echo "<option value='".$estacao['nomeLinha']." - ".$estacao['nome']."'>".$estacao['nomeLinha']." - ".$estacao['nome']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="select_estacao2">Escolha outra Estação</label>
                                    <select required name="destino" id="select_estacao2">
                                        <?php 
                                            foreach ($estacoes as $estacao) {
                                                echo "<option value='".$estacao['nomeLinha']." - ".$estacao['nome']."'>".$estacao['nomeLinha']." - ".$estacao['nome']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-default">Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="dist/js/jquery1.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            var formulario = $("#formulario");
            $("#dijkstra").on('click', function () {
                formulario.attr('action', 'dijkstra.php');
            });
        </script>
    </body>
</html>