<?php

$inicio = $_POST['inicio'];
$destino = $_POST['destino'];

function dijkstra($graph_array, $source, $target)
{
    $vertices = array();
    $neighbours = array();
    foreach ($graph_array as $edge) {
        array_push($vertices, $edge[0], $edge[1]);
        $neighbours[$edge[0]][] = array("end" => $edge[1], "cost" => $edge[2]);
        $neighbours[$edge[1]][] = array("end" => $edge[0], "cost" => $edge[2]);
    }
    $vertices = array_unique($vertices);
 
    foreach ($vertices as $vertex) {
        $dist[$vertex] = INF;
        $previous[$vertex] = null;
    }
 
    $dist[$source] = 0;
    $Q = $vertices;
    while (count($Q) > 0) {
 
        // TODO - Find faster way to get minimum
        $min = INF;
        foreach ($Q as $vertex) {
            if ($dist[$vertex] < $min) {
                $min = $dist[$vertex];
                $u = $vertex;
            }
        }
 
        $Q = array_diff($Q, array($u));
        if ($dist[$u] == INF or $u == $target) {
            break;
        }
 
        if (isset($neighbours[$u])) {
            foreach ($neighbours[$u] as $arr) {
                $alt = $dist[$u] + $arr["cost"];
                if ($alt < $dist[$arr["end"]]) {
                    $dist[$arr["end"]] = $alt;
                    $previous[$arr["end"]] = $u;
                }
            }
        }
    }
    $path = array();
    $u = $target;
    while (isset($previous[$u])) {
        array_unshift($path, $u);
        $u = $previous[$u];
    }
    array_unshift($path, $u);
    return $path;
}

function calcDistance($row)
{
    if ($row['linha1'] == $row['linha2']) {
        return 1;
    } else {
        return 1000;
    }
}

include('config.php');
$mysqli = new mysqli($db['host'], $db['user'], $db['pass'], $db['name']);
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
}
$result = $mysqli->query("SELECT a.nome as estacao1, c.nome as linha1, b.nome as estacao2, d.nome as linha2 FROM Conexoes INNER JOIN Estacoes a ON Conexoes.estacao1=a.idEstacoes INNER JOIN Estacoes b ON Conexoes.estacao2=b.idEstacoes INNER JOIN Linha c ON a.linha=c.idLinha INNER JOIN Linha d ON b.linha=d.idLinha");
$base = array();
while ($row = $result->fetch_assoc()) {
    $valor = calcDistance($row);
    $dado = array($row['linha1'].' - '.$row['estacao1'], $row['linha2'].' - '.$row['estacao2'], $valor);
    array_push($base, $dado);
}

$path = dijkstra($base, $inicio, $destino);

?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>Grafos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Trabalho de Grafos - 2014/2"/>
        <meta name="author" content="EJ-kun"/>
        <link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css"    media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="dist/css/all.css"    media="screen,projection"/>
    </head>

    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        <a class="navbar-brand" href="#"><b>Grafos</b></a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php">Aplicação</a></li>
                            <li><a href="new_page.php">Cadastrar</a></li>
                            <li><a href="view_page.php">Visualizar</a></li>
                        </ul>
                        <p class="navbar-text navbar-right">Por Everton Júnior e Thiago Baltazar</p>
                    </div>
                </div>
            </nav>
            <div class="container panel panel-default page">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Estação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i=1;
                            foreach ($path as $p) {
                                echo "<tr>";
                                echo "<td>".$i++."</td>";
                                echo "<td>".$p."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script type="text/javascript" src="dist/js/jquery1.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">
        </script>
    </body>
</html>