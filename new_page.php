<?php

include('config.php');
$mysqli = new mysqli($db['host'], $db['user'], $db['pass'], $db['name']);
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
}

if (isset($_POST['tipo'])) {
    $tipo = $_POST['tipo'];
    if ($tipo == "linha") {
        $linha = $_POST['nome_linha'];
        $mysqli->query("INSERT INTO Linha (nome) VALUES ('".$linha."')");
    } else if ($tipo == "estacao") {
        $linha = $_POST['select_linha'];
        $estacao = $_POST['nome_estacao'];
        $mysqli->query("INSERT INTO Estacoes (nome, linha) VALUES ('".$estacao."',".$linha.")");

    } else if ($tipo == "conexao") {
        $estacao1 = $_POST['select_estacao1'];
        $estacao2 = $_POST['select_estacao2'];
        $mysqli->query("INSERT INTO Conexoes (estacao1, estacao2) VALUES (".$estacao1.",".$estacao2.")");
    }
    ?>
    <script type="text/javascript">
        alert('Cadastro concluído com sucesso');
    </script
    <?php
}

$result = $mysqli->query("SELECT * FROM Linha");

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $linhas[] = $row;
    }
} else {
    $linhas = null;
}

$result = $mysqli->query("SELECT Estacoes.idEstacoes, Estacoes.nome, Linha.nome as nomeLinha FROM Estacoes INNER JOIN Linha ON Estacoes.linha=Linha.idLinha ORDER BY Estacoes.idEstacoes;");

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $estacoes[] = $row;
    }
} else {
    $estacoes = null;
}

$mysqli->close();

?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <title>Grafos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Trabalho de Grafos - 2014/2"/>
        <meta name="author" content="EJ-kun"/>
        <link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css"    media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="dist/css/all.css"    media="screen,projection"/>
    </head>

    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        <a class="navbar-brand" href="#"><b>Grafos</b></a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Aplicação</a></li>
                            <li class="active"><a href="new_page.php">Cadastrar</a></li>
                            <li><a href="view_page.php">Visualizar</a></li>
                        </ul>
                        <p class="navbar-text navbar-right">Por Everton Júnior e Thiago Baltazar</p>
                    </div>
                </div>
            </nav>
            <div class="container panel panel-default page">
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default aside">
                            <h3 style="text-align:center;">O que deseja cadastrar?</h3>
                            <br>
                            <ul class="nav nav-pills nav-stacked">
                                <li role="presentation" class="type_form active"><a id="show_linha">Linha de Metrô</a></li>
                                <li role="presentation" class="type_form"><a id="show_estacao">Estação de Metrô</a></li>
                                <li role="presentation" class="type_form"><a id="show_conexao">Conexão entre Estações</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel panel-default main">
                            <div class="linha">
                                <h4>Criar nova linha de metrô</h4>
                                <br>
                                <form action="" method="post">
                                    <input name="tipo" type="text" hidden value="linha">
                                    <div class="form-group">
                                        <label for="nome_linha">Nome da Linha</label>
                                        <input required type="text" class="form-control" id="nome_linha" name="nome_linha">
                                    </div>
                                    <button type="submit" class="btn btn-default">Enviar</button>
                                </form>
                            </div>

                            <div class="estacao">
                                <h4>Criar nova estação de metrô</h4>
                                <br>
                                <form action="" method="post">
                                    <input name="tipo" type="text" hidden value="estacao">
                                    <div class="form-group">
                                        <label for="select_linha">Escolha uma Linha</label>
                                        <select required name="select_linha" id="select_linha">
                                            <?php 
                                                foreach ($linhas as $linha) {
                                                    echo "<option value='".$linha['idLinha']."'>".$linha['nome']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nome_estacao">Nome da Estação</label>
                                        <input required type="text" class="form-control" id="nome_estacao" name="nome_estacao">
                                    </div>
                                    <button type="submit" class="btn btn-default">Enviar</button>
                                </form>
                            </div>

                            <div class="conexao">
                                <form action="" method="post">
                                    <h4>Criar nova conexão entre estações de metrô</h4>
                                    <br>
                                    <input name="tipo" type="text" hidden value="conexao">
                                    <div class="form-group">
                                        <label for="select_estacao1">Escolha uma Estação</label>
                                        <select required name="select_estacao1" id="select_estacao1">
                                            <?php 
                                                foreach ($estacoes as $estacao) {
                                                    echo "<option value='".$estacao['idEstacoes']."'>".$estacao['nome']." - ".$estacao['nomeLinha']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="select_estacao2">Escolha outra Estação</label>
                                        <select required name="select_estacao2" id="select_estacao2">
                                            <?php 
                                                foreach ($estacoes as $estacao) {
                                                    echo "<option value='".$estacao['idEstacoes']."'>".$estacao['nome']." - ".$estacao['nomeLinha']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-default">Enviar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="dist/js/jquery1.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            var form_linha = $(".linha");
            var form_estacao = $(".estacao");
            var form_conexao = $(".conexao");

            $(function() {
                form_estacao.hide();
                form_conexao.hide();
            });

            $("#show_linha").on('click', function () {
                form_estacao.slideUp();
                form_conexao.slideUp();
                form_linha.show("slow");
                $(".type_form").removeClass("active");
                $(this).parent().addClass("active");
            });

            $("#show_estacao").on('click', function () {
                form_linha.slideUp();
                form_conexao.slideUp();
                form_estacao.show("slow");
                $(".type_form").removeClass("active");
                $(this).parent().addClass("active");
            });

            $("#show_conexao").on('click', function () {
                form_linha.slideUp();
                form_estacao.slideUp();
                form_conexao.show("slow");
                $(".type_form").removeClass("active");
                $(this).parent().addClass("active");
            });
        </script>
    </body>
</html>